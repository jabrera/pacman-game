# pacman-game

A classic Pac-Man Game using Javascript from scratch

Demo: https://projects.juvarabrera.com/pacman-game

## Controls
`W` - go up \
`A` - go left \
`S` - go down \
`D` - go right
